---
title: "El proyecto"
image: "supernodo.jpg"
weight: 10
---

Ganchea.me es una red abierta y comunitaria en fase piloto cuyo objetivo es proveer de servicios a l@s vecin@s del barrio del gancho (San Pablo) en Zaragoza.

Se busca desde la tecnología trabajar valores comunitarios, de empoderamiento, participación o apropiación del espacio, desde un posicionamiento ya sea físicó o virtual en el que las personas enuentren lugar para aportar sus propias narrativas.

El proyecto se nutre de experiencias locales de organización social, alianzas vecinales así como de las diferentes entidades del barrio que le dan vida. Con la idea de a partir de ahí ir ampliando la red para cubrir el barrio.
