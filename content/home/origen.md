---
title: "Origen"
image: "liberadwifi.jpg"
weight: 30
---

Durante el confinamiento motivado por la pandemia de COVID en Marzo de 2020, el acceso a internet se convirtió en una necesidad aún más básica de lo que ya era: las clases de los colegios, reuniones, encargos... pasaron a ser online.

En el barrio, mucha gente no tiene acceso adecuado a ordenadores e internet, por lo que se vieron doblemente excluídas. Ante este problema, y otros relacionados con el confinamiento, apareció la red de apoyo del Gancho, formada por vecinos y vecinas voluntarios. Entre otras acciones se promovió la compartición de conexión wifi, recolectaron, repararon y configuraron routers y equipos donados, y los repartieron entre gente que los necesitaba.

Ganchea.me es una continuación de esa iniciativa.
