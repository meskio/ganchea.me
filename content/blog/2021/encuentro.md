---
title: "Encuentro: ¿Qué necesidades puede cubrir una red wifi comunitaria?"
date: 2021-06-09T14:14:45+02:00
---

El próximo **Sábado 19 de Junio a las 10:00** en el [C.S.C. Luis 
Buñuel](centroluisbunuel.org/) vamos a juntarnos para hablar sobre ganchea.me y 
construir el proyecto entre todas.  Tenemos la tecnología y el conocimiento para 
levantar redes, pero nos falta lo mas importante: la comunidad.

Creemos que para levantar una red como la que aspiramos necesitamos construir un 
proyecto diverso, donde estén involucradas las diferentes entidades y personas 
del barrio. Por eso hemos organizado este encuentro, para sentarnos a hablar lo 
que puede ser ganchea.me y producir cambios en lo local.

En este largo año hemos visto crecer necesidades que ya estaban ahí y crearse 
nuevas que no preveíamos. ¿Qué entiendes por brecha digital? ¿Qué problemas se 
han vivido y cuales han continuado? ¿Cuales creemos que son nuevos y cuales 
simplemente estaban invisibles? ¿Puede haber mas y no nos damos cuenta? ¿Qué 
demografías son las mas afectadas?

En esta situación, ¿Cómo puede una red wifi comunitaria ayudar? ¿A quien 
queremos conectar? ¿Qué zonas son prioritarias? ¿Qué contenidos y/o servicios 
pueden ser útiles?

¿Te apuntas? Envíanos un correo para confirmar tu asistencia a 
contacto@ganchea.me. Ya que nos escribes dedícale 5 minutos a responder alguna 
de estas preguntas y enviarnos tu reflexión, así nos facilitas el preparar la 
jornada.

Nos vemos la semana que viene 😀

{{< figure src="/img/2021/encuentro.jpg" alt="encuentro" >}}
